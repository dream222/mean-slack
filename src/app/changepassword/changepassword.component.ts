﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, UserService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'changepassword.component.html'
})

export class ChangepasswordComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    changepassword() {
        this.loading = true;
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let userModel = {"_id":"","username":"","firstName":"","lastName":"","password":""};
        userModel._id = currentUser._id;
        userModel.username = currentUser.username;
        userModel.firstName = currentUser.firstName;
        userModel.lastName = currentUser.lastName;
        userModel.password = this.model.new_password;

        this.userService.update(userModel)
            .subscribe(
                data => {console.log(this)
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
