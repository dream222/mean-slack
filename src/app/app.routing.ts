﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { ChatComponent } from './chat/index';
import { LoginComponent } from './login/index';
import { ChangepasswordComponent } from './changepassword/index';
import { RegisterComponent } from './register/index';
import { AuthGuard } from './_guards/index';

const appRoutes: Routes = [
    { path: '', component: ChatComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'changepassword', component: ChangepasswordComponent },
    { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);